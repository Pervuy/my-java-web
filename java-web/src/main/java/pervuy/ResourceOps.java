package pervuy;

import java.net.URISyntaxException;
import java.util.Objects;

public class ResourceOps {

    public static String dirUnsafe(String dir){
       try {
            String path = Objects.requireNonNull(ResourceOps.class)
                    .getClassLoader()
                    .getResource(dir)
                    .toURI()
                    .getPath();
            if (path.startsWith("/")) path = path.substring(1);
            return path;
        } catch (URISyntaxException e) {
           throw new RuntimeException(e);
        }
    }

}
