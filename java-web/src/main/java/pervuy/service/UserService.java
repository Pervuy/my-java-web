package pervuy.service;

import lombok.SneakyThrows;
import pervuy.constants.Constance;
import pervuy.dao.UserDao;
import pervuy.entity.User;
import pervuy.entity.UserLogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

public class UserService {

    private final UserDao userDAO;

    public UserService() throws SQLException {
        Connection conn = DriverManager.getConnection(Constance.urlConnection,
                                                        Constance.userConnection,
                                                        Constance.passConnection);
        this.userDAO = new UserDao(conn);
    }

    public List<User> getUsersByPage(int numberPage){
        try {
            return userDAO.getUsers(numberPage);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void saveUser(User user){
        try {
            userDAO.save(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean deleteUser(UUID id){
        try {
            return userDAO.delete(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    public Optional<User> load(UUID id){

        return userDAO.load(id);

    }

    public List<UserLogin> getLastLogin(int numberPage){
        ArrayList<UserLogin> userLogins = new ArrayList<>();
        try {
            ResultSet rs = userDAO.getLastLogin(numberPage);
            while (rs.next()){
                String login = rs.getString("login");
                String password = rs.getString("password");
                String ref = rs.getString("ref");
                String profession = rs.getString("profession");
                LocalDateTime lastLogin = rs.getTimestamp("lastLogin_time").toLocalDateTime();
                String pathPhoto = rs.getString("path_photo");
                userLogins.add(new UserLogin(new User(login, password, profession, UUID.fromString(ref), pathPhoto),lastLogin));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return userLogins;
    }

    @SneakyThrows
    public Optional<User> getUserByLogin (String login){
        return userDAO.getUserByLogin(login);
    }
    public void insertUserLoginHistory(User user){
        userDAO.insertUserLoginHistory(user);
    }
}
