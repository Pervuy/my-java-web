package pervuy.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.SneakyThrows;
import pervuy.entity.User;
import pervuy.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.UUID;

public class RegisterServlet extends HttpServlet {

    private final UserService userService;

    @SneakyThrows
    public RegisterServlet() {
        this.userService = new UserService();
    }

    // http://localhost:8080/register
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Configuration configuration = TinderConfiguration.of("templates");

        HashMap<String, Object> dataModel = new HashMap<>();

        try (PrintWriter printWriter = resp.getWriter()){
            configuration
                    .getTemplate("register.ftl")
                    .process(dataModel, printWriter);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){

        String password = req.getParameter("password");
        String email = req.getParameter("email");
        User userByLogin = userService.getUserByLogin(email).orElse(null);
        if (userByLogin != null){
            resp.sendRedirect("register");
        }else {
            UUID refUser = UUID.randomUUID();
            User user = new User(email, password, "", refUser, "");

            userService.saveUser(user);
            userService.insertUserLoginHistory(user);

            resp.addCookie(new Cookie("UUID", refUser.toString()));
            resp.sendRedirect("/users");
        }
    }
}
