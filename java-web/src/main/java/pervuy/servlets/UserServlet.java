package pervuy.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import pervuy.ResourceOps;
import pervuy.entity.UserLogin;
import pervuy.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class UserServlet extends HttpServlet {

    private final UserService userService;

    public UserServlet(){
        try {
            userService = new UserService();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // http://localhost:8080/users
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (Auth.getCookie(req).isEmpty()){
            resp.sendRedirect("/login");
        }
        Configuration configuration = TinderConfiguration.of("templates");

        HashMap<String, Object> dataModel = new HashMap<>();

        List<UserLogin> usersByPage = userService.getLastLogin(1);
        dataModel.put("users", usersByPage);

        try (PrintWriter printWriter = resp.getWriter()){
            configuration
                    .getTemplate("people-list.ftl")
                    .process(dataModel, printWriter);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

}
