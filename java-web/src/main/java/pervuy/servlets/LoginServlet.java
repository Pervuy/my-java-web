package pervuy.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.SneakyThrows;
import pervuy.entity.User;
import pervuy.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class LoginServlet extends HttpServlet {

    private final UserService userService;

    // http://localhost:8080/login
    @SneakyThrows
    public LoginServlet() {
        this.userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Configuration configuration = TinderConfiguration.of("templates");

        HashMap<String, Object> dataModel = new HashMap<>();

        try (PrintWriter printWriter = resp.getWriter()){
            configuration
                    .getTemplate("login.ftl")
                    .process(dataModel, printWriter);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String password = req.getParameter("password");
        User userByLogin = userService.getUserByLogin(req.getParameter("email")).orElse(null);
        if (userByLogin == null){
            resp.sendRedirect("login");
        }else {
            if (userByLogin.getPassword().equals(password)) {
                userService.insertUserLoginHistory(userByLogin);
                resp.addCookie(new Cookie("UUID", userByLogin.id().toString()));
                resp.sendRedirect("/users");
            }
        }


    }
}
