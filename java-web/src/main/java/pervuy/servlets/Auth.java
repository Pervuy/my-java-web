package pervuy.servlets;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class Auth {
    private static String cookieName = "UUID";

    private static Cookie[] c0 = new Cookie[0];

    public static Optional<UUID> getCookie(HttpServletRequest req){
        Cookie[] cookies = req.getCookies();
        return tryGetCookie(cookies);
    }

    private static Optional<UUID> tryGetCookie(Cookie[] cookies) {
       return Arrays.stream(cookies != null ? cookies : c0)
                .filter(c -> c.getName().equals(cookieName))
                .findAny()
                .map(Cookie::getValue)
                .map(UUID::fromString);
    }

}
