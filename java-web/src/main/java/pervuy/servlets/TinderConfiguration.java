package pervuy.servlets;

import freemarker.template.Configuration;
import pervuy.ResourceOps;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public interface TinderConfiguration {

    static Configuration of(String dirTemplates) throws IOException {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_32);
        configuration.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        configuration.setDirectoryForTemplateLoading(new File(ResourceOps.dirUnsafe(dirTemplates)));
        return configuration;
    }
}
