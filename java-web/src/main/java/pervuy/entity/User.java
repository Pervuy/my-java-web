package pervuy.entity;



import pervuy.dao.Identifiable;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class User implements Serializable, Identifiable {
    private final UUID ref;
    private final String login;
    private final String password;
    private String profession;
    private String pathPhoto;

    public User(String login, String password, String profession, UUID ref, String pathPhoto) {
        this.login = login;
        this.password = password;
        this.profession = profession;
        this.ref = ref;
        this.pathPhoto = pathPhoto;
    }

    @Override
    public UUID id() {
        return ref;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + ref +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getProfession() {
        return profession;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }
}
