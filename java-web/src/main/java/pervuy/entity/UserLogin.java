package pervuy.entity;

import pervuy.libs.EX;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserLogin extends User{

    private LocalDateTime lastLogin;
    public UserLogin(User user, LocalDateTime lastLogin){
        super(user.getLogin(),user.getPassword(),user.getProfession(),user.id(),user.getPathPhoto());
        this.lastLogin = lastLogin;
    }

    public String getLastLogin() {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        return lastLogin.format(dateTimeFormatter);

    }

    public String getDaysAgo() {
        throw EX.NI;
        //return "1 days ago";
    }
}
