package pervuy;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import pervuy.servlets.LoginServlet;
import pervuy.servlets.RegisterServlet;
import pervuy.servlets.UserServlet;

public class TinderApp {

    public void run() throws Exception {
        Server server = new Server(8080);

        ServletContextHandler handler = new ServletContextHandler();

        String osStaticLocation = ResourceOps.dirUnsafe("templates/css");
        handler.addServlet(new ServletHolder(new StaticContentServlet(osStaticLocation)), "/css/*");

        handler.addServlet(new ServletHolder(new UserServlet()),"/users");
        handler.addServlet(new ServletHolder(new LoginServlet()),"/login");
        handler.addServlet(new ServletHolder(new RegisterServlet()),"/register");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
