package pervuy;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class HtmlPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){

        String path = ResourceOps.dirUnsafe("users.html");
        Path fileWithFullPath = Path.of(path);

        if (!fileWithFullPath.toFile().exists()){
            resp.setStatus(404);
        }else {
            try (ServletOutputStream os = resp.getOutputStream()){
                Files.copy(fileWithFullPath,os);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


    }
}
