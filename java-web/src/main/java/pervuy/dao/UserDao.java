package pervuy.dao;

import lombok.SneakyThrows;
import pervuy.entity.User;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserDao implements DAO<User>{

    record Page(int number) {
        private static final int defaultPageSize = 20;
        public String toSQL(){
            return String.format("""
                OFFSET %d
                LIMIT %d
                """,
                    (number-1)*defaultPageSize,
                    defaultPageSize);
        }
    }

    private final Connection conn;

    public UserDao(Connection conn) {
        this.conn = conn;
    }

    @Override
    public boolean delete(UUID id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM users WHERE ref = ?");
        preparedStatement.setObject(1, id);
        return preparedStatement.execute();

    }

    @Override
    public void save(User user) throws SQLException {
      //if (user.id()==null){
          insert(user);
//      }else {
//          update(user);
//      }
    }

    @Override
    public Optional<User> load(UUID id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM users WHERE ref = (?)");
        preparedStatement.setString(1, id.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return Optional.of(new User(
                                        resultSet.getString("login"),
                                        resultSet.getString("password"),
                                        resultSet.getString("profession"),
                                        (UUID) resultSet.getObject("ref"),
                                        ""
                                ));
        }
        return Optional.empty();
    }

    private void insert(User user) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("""
                INSERT INTO
                    users (ref, login, password, profession, path_photo) values(?,?,?,?,?)
                ON CONFLICT (ref) DO
                     UPDATE SET
                        login = excluded.login,
                        password = excluded.password,
                        profession = excluded.profession,
                        path_photo = excluded.path_photo;
                """);
        preparedStatement.setObject(1, user.id());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getProfession());
        preparedStatement.setString(5, user.getPathPhoto());
        preparedStatement.executeUpdate();

    }
    @SneakyThrows
    public void insertUserLoginHistory(User user){
        PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO user_login_history (user_ref, login_time) values(?,?)");
        preparedStatement.setObject(1, user.id());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
        preparedStatement.execute();
    }
    private void update(User user) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("""
                        UPDATE users
                              SET
                                 login = (?),
                                 password = (?),
                                 profession = (?)
                        WHERE
                              ref = (?)
                        """);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getProfession());
        preparedStatement.setObject(4, user.id());
        preparedStatement.executeUpdate();

    }

    public List<User> getUsers(int numberPage) throws SQLException {
        ArrayList<User> users = new ArrayList<>();
        ResultSet rs = conn.prepareStatement(String.format("""
                        SELECT
                            ref,
                            login,
                            profession,
                            password
                        FROM
                            users
                        ORDER BY ref
                        %s
                        """,
                new Page(numberPage).toSQL())).executeQuery();
        while (rs.next()){
            users.add(new User(
                                rs.getString("login"),
                                rs.getString("password"),
                                rs.getString("profession"),
                                (UUID) rs.getObject("ref"),
                                ""
                        ));
        }

        return users;
    }
    public ResultSet getUsersAndLogin(int numberPage) throws SQLException {

        return conn.prepareStatement(String.format("""
                        SELECT
                            u.ref,
                            u.login,
                            u.password,
                            u.profession,
                            ulh.login_time
                        FROM
                            users as u
                              LEFT JOIN
                                user_login_history as ulh
                                on u.ref = ulh.user_ref
                        ORDER BY u.id
                        %s
                        """,
                new Page(numberPage).toSQL())).executeQuery();
    }
    public ResultSet getLastLogin(int numberPage) throws SQLException {
        return conn.prepareStatement(String.format("""
                        SELECT
                            u.ref,
                            u.login,
                            u.password,
                            u.profession,
                            u.path_photo,
                            max(ulh.login_time) as lastLogin_time
                        FROM
                            users as  u
                              LEFT JOIN user_login_history as ulh
                                on u.ref = ulh.user_ref
                        GROUP BY
                            u.ref,
                            u.login,
                            u.password,
                            u.profession,
                            u.path_photo
                        ORDER BY u.ref
                        %s
                        """,
                new Page(numberPage).toSQL())).executeQuery();
    }

    public Optional<User> getUserByLogin(String login) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("""
                SELECT
                    u.ref,
                    u.login,
                    u.password,
                    u.profession,
                    u.path_photo
                FROM
                    users as u
                WHERE
                    u.login = ?
                """);
        preparedStatement.setString(1, login);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()){
            return Optional.of(new User(
                    rs.getString("login"),
                    rs.getString("password"),
                    rs.getString("profession"),
                    (UUID) rs.getObject("ref"),
                    rs.getString("path_photo")
            ));
        }else {
            return Optional.empty();
        }
    }


}
