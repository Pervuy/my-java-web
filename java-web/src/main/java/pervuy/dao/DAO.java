package pervuy.dao;

import java.util.Optional;
import java.util.UUID;

public interface DAO<T extends Identifiable> {

    boolean delete(UUID id) throws Exception;
    default boolean delete(T t) throws Exception {
        return delete(t.id());
    }
    void save(T t) throws Exception;
    Optional<T> load(UUID id) throws Exception;
}
