package pervuy.dao;

import java.util.UUID;

public interface Identifiable {

    UUID id();
}
